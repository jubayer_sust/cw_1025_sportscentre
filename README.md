# README #

This project is for course work of module 7COM2015 : Programming for Software Engineering.

### What is this repository for? ###

This project is for University Sports Centre (USC). Student can use this application for managing the bookings of group exercise classes made by the them. The centre offers different group exercise classes on both Saturday and Sunday. The classes could be Yoga, Zumba, Aquacise, Box Fit, Body Blitz, etc. Each class can accommodate 4 students at most.

For either day (Saturday or Sunday), there are 3 classes per day: 1 in the morning, 1 in the afternoon, 1 in the evening. The price of each class is different. The class price for the same exercise will remain the same even if they run at a different time.
A student who wants to book a class needs to first check the timetable and then select a class on a day. A student can check the timetable by two ways: one is by specifying the date and the other is by specifying the exercise name. Students are allowed to change a booking, provided there are still spaces available for the newly selected class. A student can book as many classes as they want so long as there is no time conflict.
After each group exercise class, students are able to write a review of the class they have attended and provide a numerical rating of the class ranging from 1 to 5 (1: Very dissatisfied, 2: Dissatisfied, 3: Ok, 4: Satisfied, 5: Very Satisfied). The rating information will be recorded in the system.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Fazla Elahi Md Jubayer - jubayer.sust.23@gmail.com
