/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AppData;

import java.util.HashMap;
import model.Booking;

/**
 *
 * @author md.jubayer
 */
public class Constant {
    public static int userId;
    public static String[] mainMenuOptions = {"1. Book a group excercise class", 
                                                "2. Change a booking"};
    
    public static final int BOOKING_STATUS_BOOKED = 1;
    public static final int BOOKING_STATUS_ATTENDED = 2;
    public static final int BOOKING_STATUS_CANCELLED = 3;
    public static HashMap<Integer, String> mapSpace = new HashMap<>();
    
    public static boolean isFromChangeBookingClass = false;
    public static Booking changeBookedClass ;
    
    
}
