/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AppData;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.Booking;
import model.Bookings;
import model.ClassTimeTable;
import model.ClassTimeTables;
import model.ExcerciseClass;
import model.ExcerciseClasses;
import model.User;
import model.Users;

/**
 *
 * @author md.jubayer
 */
public class Database {
    
    
    private databaseLoadCallBack callBack;
    
    public static HashMap<String, ArrayList<ClassTimeTable>> mapDayNameAgainstClass = new HashMap<>();
    public static HashMap<Integer, ClassTimeTable> mapExcerciseClassTimeTablesAgaistId = new HashMap<>();
    public static HashMap<Integer, ExcerciseClass> mapExcerciseClassAgaistId = new HashMap<>();
    public static ArrayList<Booking> bookings = new ArrayList<>();
    public static List<ClassTimeTable> classTimeTables = new ArrayList<>();
    public static Users users;
    
    public void loadDatabaseFromJson(databaseLoadCallBack callBack) {
        
        
        
        this.callBack = callBack;
        loadUserList();
        
    }
    
    
    private void loadUserList(){
     ReadJsonFile readUser = new ReadJsonFile();
     
     readUser.readUserJsonFile(new ReadJsonFile.JsonFileReadCallBack() {
            @Override
            public void gotJsonInString(String json) {
                //System.out.println(json);
                Gson gson = new Gson();
                users = gson.fromJson(json, Users.class);
                
                loadExcerciseClasses();
                
            }

        });
    
    
    }
    
    private void loadExcerciseClasses() {
        
        ReadJsonFile readJson = new ReadJsonFile();
        
        readJson.readExcerciseClass(new ReadJsonFile.JsonFileReadCallBack() {
            @Override
            public void gotJsonInString(String json) {
                Gson gson = new Gson();
                ExcerciseClasses response = gson.fromJson(json, ExcerciseClasses.class);
                doMappingAgainstClassId(response);
                
                loadClassTimeTable();
            }
        });
    }
    
    private void doMappingAgainstClassId(ExcerciseClasses excerciseClassInfo){
        mapExcerciseClassAgaistId.clear();
     
        for(ExcerciseClass excerciseClass: excerciseClassInfo.getExcerciseClasses()){
        
          mapExcerciseClassAgaistId.put(excerciseClass.getId(), excerciseClass);
        }
    
    }
    
    private void loadClassTimeTable() {
        ReadJsonFile readJson = new ReadJsonFile();
        
        readJson.readClassTimeTableJsonFile(new ReadJsonFile.JsonFileReadCallBack() {
            @Override
            public void gotJsonInString(String json) {
                
                Gson gson = new Gson();
                ClassTimeTables classTimeTableInfo = gson.fromJson(json, ClassTimeTables.class);
                classTimeTables = classTimeTableInfo.getClassTimeTables();
                doMappingAgainstDayName(classTimeTableInfo);
                doMappingAgainstClassTimeTableId(classTimeTableInfo);
                
                loadBookingInfo(readJson);
                
            }
        });
    }
    
    private void loadBookingInfo(ReadJsonFile readJson) {
        
        bookings.clear();
        
        readJson.readBookingInfoJsonFile(new ReadJsonFile.JsonFileReadCallBack() {
            @Override
            public void gotJsonInString(String json) {
                
                Gson gson = new Gson();
                Bookings bookingInfo = gson.fromJson(json, Bookings.class);
                bookings.addAll(bookingInfo.getBookings());
                //doMappingAgainstDayName(classTimeTableInfo);
                loadSpaceMapingForView();
               
                
            }
        });
        
    }
    
    public void loadSpaceMapingForView(){
        Constant.mapSpace.put(1, " ");
        Constant.mapSpace.put(2, "  ");
        Constant.mapSpace.put(3, "   ");
        Constant.mapSpace.put(4, "    ");
        Constant.mapSpace.put(5, "     ");
        Constant.mapSpace.put(6, "      ");
        Constant.mapSpace.put(7, "       ");
        Constant.mapSpace.put(8, "        ");
        Constant.mapSpace.put(9, "         ");
        Constant.mapSpace.put(10, "         ");
        Constant.mapSpace.put(11, "          ");
        Constant.mapSpace.put(12, "           ");
        Constant.mapSpace.put(13, "            ");
        Constant.mapSpace.put(14, "             ");
        Constant.mapSpace.put(15, "              ");
        Constant.mapSpace.put(16, "               ");
        Constant.mapSpace.put(17, "                ");
        Constant.mapSpace.put(18, "                 ");
        Constant.mapSpace.put(19, "                  ");
        callBack.onDatabaseLoad();
    
    }
    
    private void doMappingAgainstDayName(ClassTimeTables classTimeTableInfo) {
        
        mapDayNameAgainstClass.clear();
        
        ArrayList<ClassTimeTable> list;
        
        for (ClassTimeTable classTimeTable : classTimeTableInfo.getClassTimeTables()) {
            
            if (mapDayNameAgainstClass.get(classTimeTable.getDayName()) != null) {
                
                list = mapDayNameAgainstClass.get(classTimeTable.getDayName());
                list.add(classTimeTable);
                
            } else {
                list = new ArrayList<>();
                list.add(classTimeTable);
                
            }
            
            mapDayNameAgainstClass.put(classTimeTable.getDayName(), list);
            
        }
        
    }

    private void doMappingAgainstClassTimeTableId(ClassTimeTables classTimeTableInfo) {
        
        mapExcerciseClassTimeTablesAgaistId.clear();
        
        for (ClassTimeTable classTimeTable : classTimeTableInfo.getClassTimeTables()) {
            
            mapExcerciseClassTimeTablesAgaistId.put(classTimeTable.getId(), classTimeTable);
            
        }
        
    }
    
    
    public static abstract class databaseLoadCallBack{
    
        public abstract void onDatabaseLoad();
    
    }
}
