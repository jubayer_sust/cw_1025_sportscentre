/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AppData;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


/**
 *
 * @author md.jubayer
 */
public class ReadJsonFile {
    
    public static final String UserJsonFileName = "User.json";
    public static final String ClassTimeTableFileName = "ExcerciseClassTimeTable.json";
    public static final String BookingInfoFileName = "BookingInfo.json";
    public static final String ExcerciseClassFileName = "ExcerciseClass.json";
    
    public void readUserJsonFile(JsonFileReadCallBack callback){
        
        if(callback == null) return;
        
         try {
            FileReader reader = new FileReader(UserJsonFileName);
            BufferedReader bufferedReader = new BufferedReader(reader);
 
            String line;
            String outPut = "";
 
            while ((line = bufferedReader.readLine()) != null) {
                //System.out.println(line);
                outPut  = outPut + line;
                outPut = outPut + "\n";
            }
            reader.close();
            
            callback.gotJsonInString(outPut);
 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void readExcerciseClass(JsonFileReadCallBack callback){
        
        if(callback == null) return;
        
         try {
            FileReader reader = new FileReader(ExcerciseClassFileName);
            BufferedReader bufferedReader = new BufferedReader(reader);
 
            String line;
            String outPut = "";
 
            while ((line = bufferedReader.readLine()) != null) {
                //System.out.println(line);
                outPut  = outPut + line;
                outPut = outPut + "\n";
            }
            reader.close();
            
            callback.gotJsonInString(outPut);
 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void readClassTimeTableJsonFile(JsonFileReadCallBack callback){
        
        if(callback == null) return;
        
         try {
            FileReader reader = new FileReader(ClassTimeTableFileName);
            BufferedReader bufferedReader = new BufferedReader(reader);
 
            String line;
            String outPut = "";
 
            while ((line = bufferedReader.readLine()) != null) {
                //System.out.println(line);
                outPut  = outPut + line;
                outPut = outPut + "\n";
            }
            reader.close();
            
            
            
            callback.gotJsonInString(outPut);
 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void readBookingInfoJsonFile(JsonFileReadCallBack callback){
        
        if(callback == null) return;
        
         try {
            FileReader reader = new FileReader(BookingInfoFileName);
            BufferedReader bufferedReader = new BufferedReader(reader);
 
            String line;
            String outPut = "";
 
            while ((line = bufferedReader.readLine()) != null) {
                //System.out.println(line);
                outPut  = outPut + line;
                outPut = outPut + "\n";
            }
            reader.close();
            
            
            
            callback.gotJsonInString(outPut);
 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
     public static abstract class JsonFileReadCallBack{
    
        public abstract void gotJsonInString(String json);
    
    }
    
            
    
}
