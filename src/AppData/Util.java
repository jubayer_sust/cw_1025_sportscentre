/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AppData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author md.jubayer
 */
public class Util {

    
    //expected format 12/03/1992
    public static String getWeekDayFromDateString(String dateString) throws ParseException {
        
        //String dateString = String.format("%d-%d-%d", year, month, day);
        Date date = new SimpleDateFormat("dd/M/yyyy").parse(dateString);

// Then get the day of week from the Date based on specific locale.
        String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date);

       // System.out.println(dayOfWeek);
       
       return dayOfWeek;
    }
    
    public static String getCurrentDayInString(){
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
    Date date = new Date();  
     return formatter.format(date);
    }

}
