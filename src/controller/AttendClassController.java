/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import AppData.Constant;
import AppData.Database;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Scanner;
import model.Booking;
import view.AttendClassView;
import view.EventListener.UserIntegerInputCallBack;
import view.EventListener.UserStringInputCallBack;

/**
 *
 * @author md.jubayer
 */
public class AttendClassController {

    private AttendClassView view;

    private ArrayList<Booking> userBookings = new ArrayList<>();

    public AttendClassController() {
        this.view = new AttendClassView();;
    }

    public boolean showUserAllBookedClasses() {

        //System.out.println(new Gson().toJson(Database.bookings));
        for (Booking booking : Database.bookings) {

            if (booking.getUserId() == Constant.userId && booking.getBookingStatus() == Constant.BOOKING_STATUS_BOOKED) {

                userBookings.add(booking);

            }
        }

        if (userBookings.size() > 0) {
            view.showUserBookedClasses(userBookings);
            return true;
        } else {
            view.printAlert("You don't have any booked class yet. Please book a class first!");
            return false;
        }

    }

    public void onBookedClassIdSelected(UserIntegerInputCallBack callBack) {
        view.printAlert("Please select id number of a booked class!");
        view.typeAlert("Type Id: ");
        Scanner in = new Scanner(System.in);
        int num;
        boolean loopContinue = true;
        do {
            try {
                num = in.nextInt();

                for (Booking booking : userBookings) {

                    if (booking.getId() == num) {
                        loopContinue = false;
                    }
                }

                if (loopContinue) {

                    view.printError("Invalid Input!!! Please type an valid booking Id");
                }
            } catch (Exception e) {
                in = new Scanner(System.in);
                num = 0;
                view.printError("Invalid Input!!! Please type an valid booking Id");
            }

        } while (loopContinue);

        callBack.gotIntInput(num);

    }

    public void onClassReviewGiven(UserStringInputCallBack callBack) {
        view.printAlert("Please give a review for the class!");
        view.typeAlert("Type review: ");
        Scanner in = new Scanner(System.in);
        String review;
        do {
            review = in.nextLine();

        } while (review.isEmpty());

        callBack.gotStringInput(review);

    }

    public void onClassRatingGiven(UserIntegerInputCallBack callBack) {
        view.printAlert("Please give rating between 1 to 5!");
        view.typeAlert("Type rating: ");
        Scanner in = new Scanner(System.in);
        int num;
        do {
            try {

                num = in.nextInt();

                if (num < 1 || num > 5) {

                    view.printError("Invalid Input!!! Please type an valid menu id (1 -5");
                }
            } catch (Exception e) {
                in = new Scanner(System.in);
                view.printAlert("Invalid Input!!! Only integer value allowed!!");
                num = 10;
            }

        } while ((num < 1 || num > 5));

        callBack.gotIntInput(num);

    }

    public void saveUserRating(int bookingId, String review, int rating, SaveChangesCallBack callBack) {

        for (Booking booking : Database.bookings) {

            if (booking.getId() == bookingId) {

                booking.setBookingStatus(Constant.BOOKING_STATUS_ATTENDED);
                booking.setReview(review);
                booking.setRating(String.valueOf(rating));

            }
        }

        view.printAlert("Your review for the class successfully saved!!");

        callBack.onReviewSaved();

    }

    public interface SaveChangesCallBack {

        public void onReviewSaved();

    }
}
