/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import AppData.Constant;
import AppData.Database;
import AppData.Util;
import AppData.WriteJsonFile;
import com.google.gson.Gson;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeSet;
import model.Booking;
import model.TempClassInfo;
import model.ClassTimeTable;
import model.ExcerciseClass;
import view.BookClassView;
import view.EventListener.UserIntegerInputCallBack;
import view.EventListener.UserStringInputCallBack;

/**
 *
 * @author md.jubayer
 */
public class BookClassController {

    private BookClassView view;
    //public TreeSet<String> classNames = new TreeSet<>();
    private HashMap<String, Integer> mapClassNameToPrice = new HashMap<>();

    public BookClassController() {

        this.view = new BookClassView();;

    }

    public void showBookingOptionsMenu() {
        view.showBookingOptionsMenu();
    }

    public void showClassesOptionsMenu() {
        view.showClassesOptionsMenu();
    }

    public void onBookingOptionSelected(UserIntegerInputCallBack callBack) {
        view.printAlert("Please select booking menu options from above!");
        view.typeAlert("Type Id: ");
        Scanner in = new Scanner(System.in);
        int num;
        do {
            try {
                num = in.nextInt();

                if (num < 1 || num > 2) {

                    view.printError("Invalid Input!!! Please type an valid menu id (1 - 2)");
                }
            } catch (Exception e) {
                in = new Scanner(System.in);
                num = - 1;
                view.printError("Invalid Input!!! Please type an valid menu id (1 - 2)");
            }

        } while ((num < 1 || num > 2));

        callBack.gotIntInput(num);

    }

    public void showAllClassNames() {

        Database.classTimeTables.forEach((classTimeTable) -> {
            ExcerciseClass excerciseClass = Database.mapExcerciseClassAgaistId.get(classTimeTable.getClassId());
            mapClassNameToPrice.put(excerciseClass.getName(), excerciseClass.getPrice());
            // classNames.add();
        });

        view.printClassNames(mapClassNameToPrice);

    }

    public void onClassOptionSelected(UserStringInputCallBack callBack) {
        view.printAlert("Please select which class you want to book from above list!");
        view.typeAlert("Type Id: ");
        Scanner in = new Scanner(System.in);
        int num;
        do {
            try {
                num = in.nextInt();

                if (num < 1 || num > mapClassNameToPrice.size()) {

                    view.printError("Invalid Input!!! Please type an valid menu id (1 - )" + mapClassNameToPrice.size());
                }
            } catch (Exception e) {
                in = new Scanner(System.in);
                num = -1;
                view.printError("Invalid Input!!! Please type an valid menu id (1 - )" + mapClassNameToPrice.size());
            }

        } while ((num < 1 || num > mapClassNameToPrice.size()));

        callBack.gotStringInput((String) mapClassNameToPrice.keySet().toArray()[num - 1]);

    }

    public void onDateGiven(UserStringInputCallBack callBack) {
        view.printAlert("Please give the date you want to book - format - dd/mm/yyyy ! The date must be Saturday or Sunday.");
        view.typeAlert("Type date(dd/mm/yyyy): ");
        Scanner in = new Scanner(System.in);
        String num;
        boolean flagShouldContinue = false;
        do {

            num = in.nextLine();

            //if (num < 1 || num > 2) {
            //   view.printError("Invalid Input!!! Please type an valid menu id (1 - 2)");
            //}
            try {
                String weekday = Util.getWeekDayFromDateString(num);

                if (weekday.equals("Sunday") || weekday.equals("Saturday")) {
                    flagShouldContinue = false;
                } else {
                    flagShouldContinue = true;
                    view.printAlert("Given date is " + weekday + ". Please give a date which is either Saturday or Sunday.");
                }
            } catch (ParseException ex) {
                view.printAlert("Invalid format. Valid format is dd/mm/yyyy.");
                flagShouldContinue = true;
            }
        } while (flagShouldContinue);

        callBack.gotStringInput(num);

    }

    public void onBookingIdSelected(int limit, UserIntegerInputCallBack callBack) {
        view.printAlert("Please select an booking id from above to book the class!");
        view.typeAlert("Type Id: ");
        Scanner in = new Scanner(System.in);
        int num;

        do {

            try {
                num = in.nextInt();
            } catch (Exception e) {
                num = -1;
                in = new Scanner(System.in);
            }
            //if (num < 1 || num > 2) {
            //   view.printError("Invalid Input!!! Please type an valid menu id (1 - 2)");
            //}
        } while (num < 0 || num > limit);

        callBack.gotIntInput(num);

    }

    public void showAvailableClassesBasedOnClassName(String className, String startDateString, ClassInfoCallBack callback) throws ParseException {

        Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse(startDateString);
        //LocalDate start = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        // LocalDate end = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        Calendar start = Calendar.getInstance();
        start.setTime(startDate);

        ArrayList<TempClassInfo> classInfos = new ArrayList<>();

        int loopCounter = 0;
        for (Date date = start.getTime();; start.add(Calendar.DATE, 1), date = start.getTime()) {
            // Do your job here with `date`.
            //System.out.println(date);

            SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE"); // the day of the week spelled out completely
            String dayName = simpleDateformat.format(date);
            //System.out.println(simpleDateformat.format(date));
            //System.out.println(currentCalender.get(Calendar.DAY_OF_WEEK));

            if (dayName.equalsIgnoreCase("Sunday") || dayName.equalsIgnoreCase("Saturday")) {

                ArrayList<ClassTimeTable> classTimeTables = Database.mapDayNameAgainstClass.get(dayName);

                for (ClassTimeTable classTimeTable : classTimeTables) {

                    ExcerciseClass excerciseClass = Database.mapExcerciseClassAgaistId.get(classTimeTable.getClassId());

                    if (excerciseClass.getName().equals(className)) {

                        SimpleDateFormat simpleDateformat2 = new SimpleDateFormat("dd/MM/yyyy"); // the day of the week spelled out completely
                        String dateString = simpleDateformat2.format(date);
                        //System.out.println("hello " +dateString);
                        TempClassInfo classInfo = new TempClassInfo(classTimeTable.getClassId(), classTimeTable.getId(), dateString);

                        for (Booking booking : Database.bookings) {

                            // System.out.println(classTimeTable.getId() +" " + booking.getClassTimeTableId());
                            String givenDate[] = dateString.split("/");
                            String loopDate[] = booking.getDate().split("/");

                            if ((Integer.parseInt(givenDate[0]) == Integer.parseInt(loopDate[0]))
                                    && (Integer.parseInt(givenDate[1]) == Integer.parseInt(loopDate[1]))
                                    && (Integer.parseInt(givenDate[2]) == Integer.parseInt(loopDate[2]))
                                    && (classTimeTable.getId() == booking.getClassTimeTableId())) {

                                if (booking.getUserId() == Constant.userId && booking.getBookingStatus() == Constant.BOOKING_STATUS_BOOKED) {
                                    classInfo.setIsSamePersonTryToBook(true);
                                }

                                if (booking.getBookingStatus() == Constant.BOOKING_STATUS_BOOKED) {
                                    classInfo.setCapacityAllocated(classInfo.getCapacityAllocated() + 1);
                                }

                            }

                        }

                        classInfos.add(classInfo);

                    }
                }

            }

            if (loopCounter++ > 30) {
                break;
            }
        }

        view.showAvailableClasses(classInfos, className);
        callback.onClassInfoAvailable(classInfos);

    }

    public void showAvailableClassesBasedOnDate(String weekday, String date, ClassInfoCallBack callback) {

        ArrayList<ClassTimeTable> classTimeTables = Database.mapDayNameAgainstClass.get(weekday);
        ArrayList<TempClassInfo> classInfos = new ArrayList<>();

        for (ClassTimeTable classTimeTable : classTimeTables) {

            TempClassInfo classInfo = new TempClassInfo(classTimeTable.getClassId(), classTimeTable.getId(), date);

            for (Booking booking : Database.bookings) {

                String givenDate[] = date.split("/");
                String loopDate[] = booking.getDate().split("/");

                if ((Integer.parseInt(givenDate[0]) == Integer.parseInt(loopDate[0]))
                        && (Integer.parseInt(givenDate[1]) == Integer.parseInt(loopDate[1]))
                        && (Integer.parseInt(givenDate[2]) == Integer.parseInt(loopDate[2]))
                        && (classTimeTable.getId() == booking.getClassTimeTableId())) {

                    if (booking.getUserId() == Constant.userId && booking.getBookingStatus() == Constant.BOOKING_STATUS_BOOKED) {
                        classInfo.setIsSamePersonTryToBook(true);
                    }

                    if (booking.getBookingStatus() == Constant.BOOKING_STATUS_BOOKED) {
                        classInfo.setCapacityAllocated(classInfo.getCapacityAllocated() + 1);
                    }
                }

            }

            classInfos.add(classInfo);
        }

        view.showAvailableClasses(classInfos, date, weekday);
        callback.onClassInfoAvailable(classInfos);
    }

    public int bookAClass(TempClassInfo classInfo, UserIntegerInputCallBack callBack) {

        if (classInfo.isIsSamePersonTryToBook()) {
            view.printAlert("You already booked this class. You cannot book a class twice.");

            if (callBack != null) {
                callBack.gotIntInput(2);
            }

            return 2;
        }

        if (classInfo.getCapacityAllocated() >= 4) {
            view.printAlert("Capacity exceed. You cannot book this class anymore");

            if (callBack != null) {
                callBack.gotIntInput(3);
            }

            return 3;

        }

        for (Booking booking : Database.bookings) {
            if (booking.getUserId() == Constant.userId && booking.getBookingStatus() == Constant.BOOKING_STATUS_BOOKED) {

                String bookDate[] = booking.getDate().split("/");
                String currentBookDate[] = classInfo.getDate().split("/");

                if ((Integer.parseInt(bookDate[0]) == Integer.parseInt(currentBookDate[0]))
                        && (Integer.parseInt(bookDate[1]) == Integer.parseInt(currentBookDate[1]))
                        && (Integer.parseInt(bookDate[2]) == Integer.parseInt(currentBookDate[2]))) {

                    String bookedTime = Database.mapExcerciseClassTimeTablesAgaistId.get(booking.getClassTimeTableId()).getTime();
                    String currentBookTime = Database.mapExcerciseClassTimeTablesAgaistId.get(classInfo.getClassTimeTableId()).getTime();

                    if (bookedTime.equals(currentBookTime)) {

                        String alreadyBookedClassName = Database.mapExcerciseClassAgaistId.get(booking.getClassId()).getName();
                        view.printAlert("on " + classInfo.getDate() + " at " + currentBookTime + " you already booked " + alreadyBookedClassName + ". So you cannot book this class on same time slot");

                        if (callBack != null) {
                            callBack.gotIntInput(4);
                        }

                        return 2;
                    }

                    if (booking.getClassTimeTableId() == classInfo.getClassTimeTableId()) {

                        view.printAlert("You already booked this class. You cannot book a class twice.");

                        if (callBack != null) {
                            callBack.gotIntInput(2);
                        }

                        return 2;
                    }
                }

            }

        }

        if (Constant.isFromChangeBookingClass) {
            /**
             * This condition will be true if user tries to book a class from
             * change class menu option. This means user wants to replace a
             * class with this newly selected class
             */

            for (Booking booking : Database.bookings) {
                if (booking.getId() == Constant.changeBookedClass.getId()) {
                    booking.setDate(classInfo.getDate());
                    booking.setUserId(Constant.userId);
                    booking.setClassId(classInfo.getClassId());
                    booking.setClassTimeTableId(classInfo.getClassTimeTableId());
                    booking.setBookingStatus(Constant.BOOKING_STATUS_BOOKED);
                    booking.setRating("");
                    booking.setReview("ok i am changed");
                    break;
                }
            }
            Constant.isFromChangeBookingClass = false;
            view.printAlert("Your " + Database.mapExcerciseClassAgaistId.get(Constant.changeBookedClass.getClassId()).getName() + " on " + Constant.changeBookedClass.getDate() + " has been replaced with " + classInfo.getClassName() + " on " + classInfo.getDate());
        } else {
            int idForDatabase = Database.bookings.size() + 1;

            Booking booking = new Booking();
            booking.setId(idForDatabase);
            booking.setDate(classInfo.getDate());
            booking.setUserId(Constant.userId);
            booking.setClassId(classInfo.getClassId());
            booking.setClassTimeTableId(classInfo.getClassTimeTableId());
            booking.setBookingStatus(Constant.BOOKING_STATUS_BOOKED);
            booking.setRating("NA");
            booking.setReview("NA");
            Database.bookings.add(booking);

            view.printAlert("Congratulations!! " + classInfo.getClassName() + " on " + classInfo.getDate() + " at " + classInfo.getWeekDay() + " " + classInfo.getDayTime() + " successfuly booked.");
        }

        Gson gson = new Gson();
        String bookingContent = gson.toJson(Database.bookings);
        if (callBack != null) {
            WriteJsonFile.writeToBookingJson(bookingContent);
        }

        //System.out.println("Booking confirmed!");
        if (callBack != null) {
            callBack.gotIntInput(1);
        }

        return 1;
    }

    public interface ClassInfoCallBack {

        public void onClassInfoAvailable(ArrayList<TempClassInfo> classInfos);
    }

}
