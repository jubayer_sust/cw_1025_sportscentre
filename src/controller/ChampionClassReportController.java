/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import AppData.Constant;
import AppData.Database;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Booking;
import model.ExcerciseClass;
import view.ChampionClassReportView;
import view.EventListener.ShowReportCallBack;
import view.EventListener.UserIntegerInputCallBack;

/**
 *
 * @author md.jubayer
 */
public class ChampionClassReportController {

    private ChampionClassReportView view;

    public ChampionClassReportController() {

        view = new ChampionClassReportView();
    }

    public void onMonthNumberSelected(UserIntegerInputCallBack callBack) {
        view.printAlert("Please give an month number ( 1 - 12) ");
        view.typeAlert("Type month number: ");
        Scanner in = new Scanner(System.in);
        int num;
        do {
            try{
            num = in.nextInt();

            if (num < 1 || num > 12) {

                view.printError("Invalid Input!!!  Please type an valid integer month number");
            }
            }catch(Exception e){
                in = new Scanner(System.in);
                view.printError("Invalid Input!!! Please type an valid menu id (1 - 12");
            num = - 12;
            }
            

        } while ((num < 1 || num > 12));

        callBack.gotIntInput(num);

    }

    public void showMonthlyReportBasedOnMonthNum(int monthNumber, ShowReportCallBack callBack) {

        HashMap<Integer, Float> map = new HashMap<>();

        float income = 0f;
        for (Booking booking : Database.bookings) {
            income = 0f;
            try {
                Date bookedDate = new SimpleDateFormat("dd/MM/yyyy").parse(booking.getDate());

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(bookedDate);

                int month = calendar.get(Calendar.MONTH) + 1;

                if (month == monthNumber && booking.getBookingStatus() == Constant.BOOKING_STATUS_ATTENDED) {

                    //ArrayList<Booking> list = new ArrayList<>();
                    if (map.get(booking.getClassId()) != null) {

                        income = map.get(booking.getClassId());
                    }

                    ExcerciseClass excerciseClass = Database.mapExcerciseClassAgaistId.get(Database.mapExcerciseClassTimeTablesAgaistId.get(booking.getClassTimeTableId()).getClassId());
                     System.out.println(excerciseClass.getName() + " " + excerciseClass.getPrice());
                    income = income + excerciseClass.getPrice();

                    map.put(booking.getClassId(), income);

                }
            } catch (ParseException ex) {
                Logger.getLogger(MonthlyClassReportController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        if (map.isEmpty()) {
            view.printAlert("No data found for this month. Please try with other month.");
            callBack.onShowed();
        } else if (map.size() == 1) {
            view.showChampionClassReport(map);
            callBack.onShowed();
        } else {

            float max = Integer.MIN_VALUE;
            float secondMax = Integer.MIN_VALUE;
            int maxIncomeId = 1;
            int secondMaxIncomeId = 2;

            for (Map.Entry<Integer, Float> entry : map.entrySet()) {
                // System.out.println(entry.getKey() + " = " + entry.getValue());

                //System.out.println("");
                if (entry.getValue() > max) {

                    secondMax = max;
                    max = entry.getValue();

                    secondMaxIncomeId = maxIncomeId;
                    maxIncomeId = entry.getKey();
                } else if (entry.getValue() > secondMax && entry.getValue() < max) {

                    secondMax = entry.getValue();
                    secondMaxIncomeId = entry.getKey();

                }
            }

            view.showChampionClassReport(map, maxIncomeId, secondMaxIncomeId);
            callBack.onShowed();

        }

    }
}
