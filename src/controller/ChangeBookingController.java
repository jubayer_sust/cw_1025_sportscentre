/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import AppData.Constant;
import AppData.Database;
import cw_1025_sportscentre.BookClass;
import java.util.ArrayList;
import java.util.Scanner;
import model.Booking;
import view.ChangeBookingView;
import view.EventListener.UserIntegerInputCallBack;

/**
 *
 * @author md.jubayer
 */
public class ChangeBookingController {

    private ChangeBookingView view;

    private ArrayList<Booking> userBookings = new ArrayList<>();

    public ChangeBookingController() {

        this.view = new ChangeBookingView();;
    }

    public boolean showUserAllBookedClasses() {
        for (Booking booking : Database.bookings) {

            if (booking.getUserId() == Constant.userId && booking.getBookingStatus() == Constant.BOOKING_STATUS_BOOKED) {

                userBookings.add(booking);

            }
        }
        if (userBookings.size() > 0) {
            view.showUserBookedClasses(userBookings);
            return true;

        } else {
            view.printAlert("You don't have any booked class yet. Please book a class first!");
            return false;
        }
    }

    public void onBookedClassIdSelected(UserIntegerInputCallBack callBack) {
        view.printAlert("Please select id number of a booked class!");
        view.typeAlert("Type Id: ");
        Scanner in = new Scanner(System.in);
        int num;
        boolean loopContinue = true;
        do {
            try {

                num = in.nextInt();

                for (Booking booking : userBookings) {

                    if (booking.getId() == num) {
                        loopContinue = false;
                    }
                }

                if (loopContinue) {

                    view.printError("Invalid Input!!! Please type an valid booking Id");
                }

            } catch (Exception e) {
                in = new Scanner(System.in);
                num = 0;
                view.printError("Invalid Input!!! Please type an valid booking Id");
            }

        } while (loopContinue);

        callBack.gotIntInput(num);

    }

    public void showChangeBookingOption() {

        view.changeBookingOption();

    }

    public void onBookingChangeOptionSelected(UserIntegerInputCallBack callBack) {
        view.printAlert("Please select an id from above option( 1 or 2)!");
        view.typeAlert("Type Id: ");
        Scanner in = new Scanner(System.in);
        int num;
        do {
            try {
                num = in.nextInt();

                if (num < 1 || num > 2) {

                    view.printError("Invalid Input!!! Please type an valid menu id (1 -5");
                }

            } catch (Exception e) {
                in = new Scanner(System.in);
                view.printError("Invalid Input!!! Please type an valid menu id (1 -5");
                num = -1;
            }

        } while ((num < 1 || num > 2));

        callBack.gotIntInput(num);

    }

    public void changeBookedClass(int bookingId) {

        for (Booking booking : Database.bookings) {

            if (booking.getId() == bookingId) {

                //booking.setBookingStatus(Constant.BOOKING_STATUS_CANCELLED);
                view.printAlert("In order to replace this booking please select a new class to replace with.");

                Constant.isFromChangeBookingClass = true;

                Booking wantToChangeBook = new Booking();
                wantToChangeBook.setId(booking.getId());
                wantToChangeBook.setDate(booking.getDate());
                wantToChangeBook.setUserId(booking.getUserId());
                wantToChangeBook.setClassId(booking.getClassId());
                wantToChangeBook.setClassTimeTableId(booking.getClassTimeTableId());
                wantToChangeBook.setBookingStatus(booking.getBookingStatus());
                wantToChangeBook.setRating(booking.getRating());
                wantToChangeBook.setReview(booking.getReview());
                Constant.changeBookedClass = wantToChangeBook;

                BookClass bookClass = new BookClass(1);
                bookClass.bringFront();

                break;

            }
        }

    }

    public void cancelBookedClass(int bookingId, OnBookingChangeCallBack callBack) {

        for (Booking booking : Database.bookings) {

            if (booking.getId() == bookingId) {

                booking.setBookingStatus(Constant.BOOKING_STATUS_CANCELLED);

            }
        }

        view.printAlert("You successfully cancelled the class");

        callBack.onBookCanceled();
    }

    public interface OnBookingChangeCallBack {

        public void onBookCanceled();

        public void onBookReplaced();

    }
}
