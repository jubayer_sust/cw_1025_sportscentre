/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Scanner;
import view.EventListener.UserIntegerInputCallBack;
import view.IntermidiateMenuView;

/**
 *
 * @author md.jubayer
 */
public class IntermidiateMenuController {

    private IntermidiateMenuView view;

    public IntermidiateMenuController() {

        view = new IntermidiateMenuView();
    }

    public void showIntermidiateMenu() {
        view.showIntermidiateMenu();
    }

    public void takeUserInput(UserIntegerInputCallBack callBack) {

        view.printAlert("Please select an option from above. (Type the id)");
        view.typeAlert("Type menu Id: ");
        Scanner scanner = new Scanner(System.in);
        int id;
        do {

            try {
                id = scanner.nextInt();

                if (id < 1 || id > 2) {

                    view.printAlert("Invalid Input. Only integer input (1 - 2) is applicable");

                }
            } catch (Exception e) {
                scanner = new Scanner(System.in);
                id = -1;
                view.printAlert("Invalid Input. Only integer input (1 - 2) is applicable");
            }

        } while (id < 1 || id > 2);

        callBack.gotIntInput(id);
    }

}
