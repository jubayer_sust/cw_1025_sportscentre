/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Scanner;
import view.EventListener.UserIntegerInputCallBack;
import view.MainMenuView;

/**
 *
 * @author md.jubayer
 */
public class MainMenuController {

    MainMenuView view;

    public MainMenuController() {

        this.view = new MainMenuView();;
    }

    public void showMainMenu() {
        view.showMainMenus();
    }

    public void onOptionItemSelected(UserIntegerInputCallBack callBack) {
        view.printAlert("Please give one of the menu id from above options!");
        view.typeAlert("Type menu Id: ");
        Scanner in = new Scanner(System.in);
        int num;
        do {
            try {
                num = in.nextInt();

                if (num < 1 || num > 6) {

                    view.printError("Invalid Input!!! Please type an valid menu id (1 - 6)");
                }
            } catch (Exception e) {
                in = new Scanner(System.in);
                num = -1;
                view.printError("Invalid Input!!! Please type an valid menu id (1 - 6)");
            }

        } while ((num < 1 || num > 6));

        callBack.gotIntInput(num);

    }

}
