/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import AppData.Constant;
import AppData.Database;
import com.google.gson.Gson;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Booking;
import model.TempClassInfo;
import view.EventListener.ShowReportCallBack;
import view.EventListener.UserIntegerInputCallBack;
import view.MonthlyClassReportView;

/**
 *
 * @author md.jubayer
 */
public class MonthlyClassReportController {

    private MonthlyClassReportView view;

    public MonthlyClassReportController() {
        view = new MonthlyClassReportView();
    }

    public void onMonthNumberSelected(UserIntegerInputCallBack callBack) {
        view.printAlert("Please give an month number ( 1 - 12) ");
        view.typeAlert("Type month number: ");
        Scanner in = new Scanner(System.in);
        int num;
        do {
            try {
                num = in.nextInt();

                if (num < 1 || num > 12) {

                }
            } catch (Exception e) {
                in = new Scanner(System.in);
                view.printError("Invalid Input!!! Please type an valid menu id (1 - 12");
                num = -1;
            }

        } while ((num < 1 || num > 12));

        callBack.gotIntInput(num);

    }

    public void showMonthlyReportBasedOnMonthNum(int monthNumber, ShowReportCallBack callBack) {

        HashMap<Integer, ArrayList<Booking>> map = new HashMap<>();

        for (Booking booking : Database.bookings) {

            try {
                Date bookedDate = new SimpleDateFormat("dd/MM/yyyy").parse(booking.getDate());

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(bookedDate);

                int month = calendar.get(Calendar.MONTH) + 1;

                if (month == monthNumber && booking.getBookingStatus() == Constant.BOOKING_STATUS_ATTENDED) {

                    ArrayList<Booking> list = new ArrayList<>();
                    if (map.get(booking.getClassId()) != null) {

                        list = map.get(booking.getClassId());
                    }
                    list.add(booking);

                    map.put(booking.getClassId(), list);

                }
            } catch (ParseException ex) {
                Logger.getLogger(MonthlyClassReportController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        if (map.isEmpty()) {
            view.printAlert("No data found for this month. Please try with other month.");
            callBack.onShowed();
        } else {
            view.showMonthlyReport(map);
            callBack.onShowed();
        }

    }

}
