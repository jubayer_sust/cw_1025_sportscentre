/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import AppData.Database;
import java.util.Scanner;
import model.Users;
import view.EventListener.UserIntegerInputCallBack;
import view.UserListView;

/**
 *
 * @author md.jubayer
 */
public class UserListController {

    private final Users model;
    private final UserListView view;

    public UserListController() {
        this.model = Database.users;
        this.view = new UserListView();
    }

    public void updateView() {

        view.printUsersList(model);
    }

    public void onOptionItemSelected(UserIntegerInputCallBack callBack) {
        view.printAlert("Please select your user id from above list!");
        view.typeAlert("Type Id: ");
        Scanner in = new Scanner(System.in);
        int num;
        do {
            try {
                num = in.nextInt();

                if (num < 1 || num > 10) {

                    view.printError("Invalid Input!!! Please select an valid user id");
                }
            } catch (Exception e) {
                in = new Scanner(System.in);
                num = -1;
                view.printError("Invalid Input!!! Please select an valid user id");
            }

        } while ((num < 1 || num > 10));

        callBack.gotIntInput(num);

    }

}
