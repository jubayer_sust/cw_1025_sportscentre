/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw_1025_sportscentre;

import AppData.Constant;
import AppData.Database;
import AppData.WriteJsonFile;
import com.google.gson.Gson;
import controller.AttendClassController;
import controller.BookClassController;
import controller.IntermidiateMenuController;
import view.AttendClassView;
import view.EventListener.UserIntegerInputCallBack;
import view.EventListener.UserStringInputCallBack;

/**
 *
 * @author md.jubayer
 */
public class AttendClass implements AttendClassController.SaveChangesCallBack {

    private AttendClassController controller;

    public void bringFront() {

        

        controller = new AttendClassController();
       boolean isAnyBookedClass =  controller.showUserAllBookedClasses();
       if(isAnyBookedClass){
        prepareUserInputForSelectBookedClassId();
       }else{
           IntermidiateMenu menu = new IntermidiateMenu();
           menu.bringFront();
       }
       
        //controller.showBookingOptionsMenu();
        //prepareUserInputForSelectBookingOption();

    }

    private void prepareUserInputForSelectBookedClassId() {
        controller.onBookedClassIdSelected(new UserIntegerInputCallBack() {
            @Override
            public void gotIntInput(int id) {
                //System.out.println(id);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

                //showActivityBasedOnMenuSelection(id);
                prepareUserInputForGetReview(id);
            }

        });

    }

    private void prepareUserInputForGetReview(int bookedId) {

        controller.onClassReviewGiven(new UserStringInputCallBack() {
            @Override
            public void gotStringInput(String num) {

                prepareUserInputForGetRating(bookedId, num);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });

    }

    private void prepareUserInputForGetRating(int bookedId, String review) {

        controller.onClassRatingGiven(new UserIntegerInputCallBack() {
            @Override
            public void gotIntInput(int num) {
                controller.saveUserRating(bookedId, review, num, AttendClass.this);
            }
        });

    }

    @Override
    public void onReviewSaved() {

        //MainMenu mainMenu = new MainMenu(Constant.userId);

       // mainMenu.bringFront();
       
       Gson gson = new Gson();
        String bookingContent = gson.toJson(Database.bookings);
        
            WriteJsonFile.writeToBookingJson(bookingContent);
        
       
       IntermidiateMenu menu = new IntermidiateMenu();
       menu.bringFront();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
