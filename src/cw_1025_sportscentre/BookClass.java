/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw_1025_sportscentre;

import AppData.Util;
import controller.BookClassController;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.TempClassInfo;
import view.EventListener.UserIntegerInputCallBack;

/**
 *
 * @author md.jubayer
 */
public class BookClass {

    ArrayList<TempClassInfo> classInfos = new ArrayList<>();
    private static final int MENU_BY_DATE = 1;
    private static final int MENU_BY_CLASS = 2;

    private BookClassController controller;
    private int selectedUserId;

    public BookClass(int selectedUserId) {
        this.selectedUserId = selectedUserId;
    }

    public void bringFront() {

        

        controller = new BookClassController();
        controller.showBookingOptionsMenu();
        prepareUserInputForSelectBookingOption();
        

    }

    private void prepareUserInputForSelectBookingOption() {
        controller.onBookingOptionSelected(new UserIntegerInputCallBack() {
            @Override
            public void gotIntInput(int id) {
                //System.out.println(id);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

                showActivityBasedOnMenuSelection(id);
            }

        });

    }

    private void prepareUserInputForTypeClassName() {
        controller.onClassOptionSelected((String className) -> {
            showAvailableClassInformationBasedOnClassName(className);
        });
    }

    private void prepareUserInputForTakeDate() {
        controller.onDateGiven((String date) -> {
            try {
                //System.out.println(date);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

                //showActivityBasedOnMenuSelection(id);
                String weekday = Util.getWeekDayFromDateString(date);
                System.out.println(weekday);
                showAvailableClassInformationBasedOnDate(weekday, date);

            } catch (ParseException ex) {
                Logger.getLogger(BookClass.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    private void showAvailableClassInformationBasedOnDate(String weekday, String date) {

        controller.showAvailableClassesBasedOnDate(weekday, date, (ArrayList<TempClassInfo> classInfos) -> {
            this.classInfos = classInfos;
            prepareUserInputForSelectBookingClassId();
        });

    }
    
    private void showAvailableClassInformationBasedOnClassName(String className){
    
        try {
            controller.showAvailableClassesBasedOnClassName(className, Util.getCurrentDayInString(), (ArrayList<TempClassInfo> classInfos) -> {
                //this.classInfos = classInfos;
                 this.classInfos = classInfos;
                prepareUserInputForSelectBookingClassId();
            });
        } catch (ParseException ex) {
            Logger.getLogger(BookClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void prepareUserInputForSelectBookingClassId() {
        controller.onBookingIdSelected(classInfos.size(), new UserIntegerInputCallBack() {
            @Override
            public void gotIntInput(int id) {
                //System.out.println(id);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

                //showActivityBasedOnMenuSelection(id);
                bookAClass(id);
            }

        });
    }

    private void bookAClass(int bookingId) {

        controller.bookAClass(classInfos.get(bookingId - 1), new UserIntegerInputCallBack() {
            @Override
            public void gotIntInput(int id) {
                if(id >= 1){
                
                    IntermidiateMenu menu = new IntermidiateMenu();
                    menu.bringFront();
                
                }
               
            }
        });
    }

    public void showActivityBasedOnMenuSelection(int id) {

        switch (id) {

            case MENU_BY_DATE:
                prepareUserInputForTakeDate();
                break;
            case MENU_BY_CLASS:
                controller.showAllClassNames();
                //controller.showClassesOptionsMenu();
                prepareUserInputForTypeClassName();
                break;
        }

    }

}
