/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw_1025_sportscentre;

import controller.ChampionClassReportController;
import view.EventListener.ShowReportCallBack;

/**
 *
 * @author md.jubayer
 */
public class ChampionClassReport {
    private ChampionClassReportController controller;
    
    
    public void bringFront(){
    
        controller = new ChampionClassReportController();
        
        prepareUserInputForSelectMonthNumber();
    
    }
    
    
    private void prepareUserInputForSelectMonthNumber(){
    
        
        controller.onMonthNumberSelected((int id) -> {
            //System.out.println(id);
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            
            //showActivityBasedOnMenuSelection(id);
            
            // controller.showChangeBookingOption();
            //prepareUserInputForSelectChangeOption(id);
            showChampionClassReport(id);
        });
    
    
    }
    
    private void showChampionClassReport(int monthNumber){
    
        controller.showMonthlyReportBasedOnMonthNum(monthNumber, new ShowReportCallBack() {
            @Override
            public void onShowed() {
                //MainMenu mainMenu = new MainMenu(1);
                //mainMenu.bringFront();
                IntermidiateMenu menu = new IntermidiateMenu();
                menu.bringFront();
            }
        });
     
    }
}
