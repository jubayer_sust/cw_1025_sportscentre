/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw_1025_sportscentre;

import AppData.Constant;
import controller.ChangeBookingController;
import view.ChangeBookingView;
import view.EventListener.UserIntegerInputCallBack;

/**
 *
 * @author md.jubayer
 */
public class ChangeBooking implements ChangeBookingController.OnBookingChangeCallBack{
    
    private ChangeBookingController controller;
    
    public void bringFront(){
        
       
     
        controller = new ChangeBookingController();
      boolean isAnyBookedClass =  controller.showUserAllBookedClasses();
        if(isAnyBookedClass){
        prepareUserInputForSelectBookedClassId();
        }else{
        IntermidiateMenu menu = new IntermidiateMenu();
           menu.bringFront();
        }
    
    }
     private void prepareUserInputForSelectBookedClassId() {
        controller.onBookedClassIdSelected(new UserIntegerInputCallBack() {
            @Override
            public void gotIntInput(int id) {
                //System.out.println(id);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

                //showActivityBasedOnMenuSelection(id);
               
                controller.showChangeBookingOption();
                prepareUserInputForSelectChangeOption(id);
            }

        });

    }
     
     
     private void prepareUserInputForSelectChangeOption(int bookingId){
     
         controller.onBookingChangeOptionSelected(new UserIntegerInputCallBack() {
             @Override
             public void gotIntInput(int num) {
                if(num ==1 ){
                    
                        controller.changeBookedClass(bookingId);
                
                
                }else if(num == 2){
                    
                    controller.cancelBookedClass(bookingId, ChangeBooking.this);
                 
                }
             }
         });
         
     
     
     }

    @Override
    public void onBookCanceled() {
         //MainMenu mainMenu = new MainMenu(Constant.userId);
        // mainMenu.bringFront();
        
        IntermidiateMenu menu = new IntermidiateMenu();
        menu.bringFront();
    }

    @Override
    public void onBookReplaced() {
       
    }
}
