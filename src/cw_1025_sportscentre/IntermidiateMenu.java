/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw_1025_sportscentre;

import controller.IntermidiateMenuController;
import view.EventListener.UserIntegerInputCallBack;

/**
 *
 * @author md.jubayer
 */
public class IntermidiateMenu {
    
    
    IntermidiateMenuController controller;
    
    public IntermidiateMenu(){
    
    
    }
    
    public void bringFront(){
    
        controller = new IntermidiateMenuController();
        controller.showIntermidiateMenu();
        prepareInputForTakingMenuId();
    
    }
    
    private void prepareInputForTakingMenuId(){
    
        controller.takeUserInput(new UserIntegerInputCallBack() {
            @Override
            public void gotIntInput(int num) {
                if(num == 1){
                
                    MainMenu menu = new MainMenu(1);
                    menu.bringFront();
                }
            }
        });
    
    }
    
    
    
    
}
