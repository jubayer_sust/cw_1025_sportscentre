/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw_1025_sportscentre;

import AppData.Constant;
import controller.MainMenuController;

/**
 *
 * @author md.jubayer
 */
public class MainMenu {

    private final int selectedUserId;
    private MainMenuController controller;
    
    public static final int MENU_BOOK_CLASS_ROOM = 1;
    public static final int MENU_CHANGE_BOOK = 2;
    public static final int MENU_ATTEND_CLASS = 3;
    public static final int MENU_MONTHLY_REPORT = 4;
    public static final int MENU_CHAMPION_CLASS = 5;
    public static final int MENU_EXIT = 6;

    public MainMenu(int selectedUserId) {

        this.selectedUserId = selectedUserId;
    }

    public void bringFront() {

        

        controller = new MainMenuController();
        controller.showMainMenu();

        prepareUserInput();

    }

    private void prepareUserInput() {
        controller.onOptionItemSelected((int id) -> {
            // System.out.println(id);
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            
            
            showActivityBasedOnMenuSelection(id);
        });
    }
    
    
    public void showActivityBasedOnMenuSelection(int id){
    
        switch(id){
        
            case MENU_BOOK_CLASS_ROOM:
                
                Constant.isFromChangeBookingClass= false;
                 BookClass bookClass = new BookClass(id);
                 bookClass.bringFront();
                
                break;
            case MENU_CHANGE_BOOK:
                ChangeBooking changeBooking = new ChangeBooking();
                changeBooking.bringFront();
                break;
                
            case MENU_ATTEND_CLASS:
                AttendClass attendClass = new AttendClass();
                attendClass.bringFront();
                break;
                
            case MENU_MONTHLY_REPORT:
                    
                MonthlyClassReport monthlyClassReport = new MonthlyClassReport();
                monthlyClassReport.bringFront();
                break;
                
            case MENU_CHAMPION_CLASS:
                ChampionClassReport championClassReport = new ChampionClassReport();
                championClassReport.bringFront();
                break;
                
             case MENU_EXIT:
                //TODO
                break;
        }
    
    }
}
