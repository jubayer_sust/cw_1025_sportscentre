/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw_1025_sportscentre;

import controller.MonthlyClassReportController;
import view.EventListener.ShowReportCallBack;

import view.EventListener.UserIntegerInputCallBack;

/**
 *
 * @author md.jubayer
 */
public class MonthlyClassReport {
    
    private MonthlyClassReportController controller;
    
    
    public void bringFront(){
    
        controller = new MonthlyClassReportController();
        
        prepareUserInputForSelectMonthNumber();
    
    }
    
    
    private void prepareUserInputForSelectMonthNumber(){
    
        
        controller.onMonthNumberSelected(new UserIntegerInputCallBack() {
            @Override
            public void gotIntInput(int id) {
                //System.out.println(id);
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

                //showActivityBasedOnMenuSelection(id);
               
               // controller.showChangeBookingOption();
                //prepareUserInputForSelectChangeOption(id);
                //controller.showMonthlyReportBasedOnMonthNum(id);
                showMonthlyClassReport(id);
            }

        });
    
    
    }
    
    
    private void showMonthlyClassReport(int monthNumber){
    
    
        controller.showMonthlyReportBasedOnMonthNum(monthNumber, new ShowReportCallBack() {
            @Override
            public void onShowed() {
                    
                    //MainMenu mainMenu = new MainMenu(1);
                    //mainMenu.bringFront();
                    IntermidiateMenu menu = new IntermidiateMenu();
                    menu.bringFront();
            }
        });
    }
    
}
