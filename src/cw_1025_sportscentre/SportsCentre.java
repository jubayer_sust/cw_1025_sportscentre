/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cw_1025_sportscentre;

import AppData.Constant;
import AppData.Database;
import AppData.WriteJsonFile;
import controller.UserListController;

/**
 *
 * @author md.jubayer
 */
public class SportsCentre {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
       
        Database database = new Database();
        database.loadDatabaseFromJson(new Database.databaseLoadCallBack() {
            @Override
            public void onDatabaseLoad() {
               // UserListView userListView = new UserListView();
                UserListController controller = new UserListController();
                controller.updateView();
                
                controller.onOptionItemSelected((int id) -> {
                    //System.out.println(id);
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    Constant.userId = id;
                    MainMenu mainMenu = new MainMenu(id);
                    mainMenu.bringFront();
                });
            }
        });
        
        
    }
    
   

}
