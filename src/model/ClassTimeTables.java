/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *
 * @author md.jubayer
 */
public class ClassTimeTables {

    @SerializedName("ClassTimeTables")
    @Expose
    private List<ClassTimeTable> classTimeTables = null;

    public List<ClassTimeTable> getClassTimeTables() {
        return classTimeTables;
    }

    public void setClassTimeTables(List<ClassTimeTable> classTimeTables) {
        this.classTimeTables = classTimeTables;
    }

}
