/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author md.jubayer
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExcerciseClasses {

    @SerializedName("ExcerciseClasses")
    @Expose
    private List<ExcerciseClass> excerciseClasses = null;

    public List<ExcerciseClass> getExcerciseClasses() {
        return excerciseClasses;
    }

    public void setExcerciseClasses(List<ExcerciseClass> excerciseClasses) {
        this.excerciseClasses = excerciseClasses;
    }

}
