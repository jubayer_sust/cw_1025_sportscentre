/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import AppData.Constant;
import AppData.Database;

/**
 *
 * @author md.jubayer
 */
public class TempClassInfo {
    
    
     private int classId;
     private int classTimeTableId;
     private String date;
     private int capacityAllocated;
     private boolean isSamePersonTryToBook = false;

    public TempClassInfo( int classId, int classTimeTableId,   String date) {
        this.capacityAllocated = 0;
       
        this.classId = classId;
        this.classTimeTableId = classTimeTableId;
       
        this.date = date;
        
    }

    public String getDayTime() {
        return Database.mapExcerciseClassTimeTablesAgaistId.get(classTimeTableId).getTime();
    }

    
    
    

    public String getWeekDay() {
        return Database.mapExcerciseClassTimeTablesAgaistId.get(classTimeTableId).getDayName();
    }

    
     
   

    public int getCapacityAllocated() {
        return capacityAllocated;
    }

    public void setCapacityAllocated(int capacityAllocated) {
        this.capacityAllocated = capacityAllocated;
    }
     
     

  
    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getUserId() {
        return Constant.userId;
    }

    

    public String getClassName() {
        return Database.mapExcerciseClassAgaistId.get(classId).getName();
    }

    

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isIsSamePersonTryToBook() {
        return isSamePersonTryToBook;
    }

    public void setIsSamePersonTryToBook(boolean isSamePersonTryToBook) {
        this.isSamePersonTryToBook = isSamePersonTryToBook;
    }

    public int getClassTimeTableId() {
        return classTimeTableId;
    }

    public void setClassTimeTableId(int classTimeTableId) {
        this.classTimeTableId = classTimeTableId;
    }
     
     
     
}
