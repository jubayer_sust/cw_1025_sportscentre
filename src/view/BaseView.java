/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.util.HashMap;

/**
 *
 * @author md.jubayer
 */
public abstract class BaseView {
    
   
    public void printError(String errorMessage){
    
        System.out.println("\n"+errorMessage);
    }
    
    
    public void printAlert(String alertMessage){
    
        System.out.println("\n"+alertMessage);
    }

    
    void clearTop() {
        System.out.print("\b\b\b\b\b");
    }
    
    public void typeAlert(String instruction){
        System.out.print(instruction);
    }
}
