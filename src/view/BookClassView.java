/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import AppData.Constant;
import AppData.Database;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import model.Booking;
import model.ExcerciseClass;
import model.TempClassInfo;

/**
 *
 * @author md.jubayer
 */
public class BookClassView extends BaseView {

    public void showBookingOptionsMenu() {
        clearTop();

        System.out.println("\nBooking options : ");

        System.out.println("1. By Date");

        System.out.println("2. By Class Name");

        // System.out.println("3. Attend a class");
    }

    //public void show
    public void showClassesOptionsMenu() {
        clearTop();

        System.out.println("\nClasses options : ");

        System.out.println("1. Yoga");

        System.out.println("2. Jim");

        // System.out.println("3. Attend a class");
    }

    public void showAvailableClasses(ArrayList<TempClassInfo> classInfos, String date, String weekDay) {

        System.out.println("\nAvailable classes for " + date + " on " + weekDay);

        System.out.println("ID---Class Name------Time----------Available Seats");

        int count = 1;
        for (TempClassInfo classInfo : classInfos) {
            String availability = "";
            if ((4 - classInfo.getCapacityAllocated()) <= 0) {
                availability = "Full Booked!";
            } else {
                availability = "available seats = " + (4 - classInfo.getCapacityAllocated());
            }
            
            String samePersonAlert = "";
            if (classInfo.isIsSamePersonTryToBook()) {
                samePersonAlert = " (You already booked this class)";
            }
            
            System.out.print(count++ +"/" + Constant.mapSpace.get( 5 - String.valueOf(count).length()));
            System.out.print(classInfo.getClassName()  + Constant.mapSpace.get( 16 - classInfo.getClassName().length()));
            System.out.print(classInfo.getDayTime()  + Constant.mapSpace.get( 14 - classInfo.getDayTime().length()));
            System.out.print(availability + samePersonAlert + "\n");
            
            
           // System.out.println(count++ + "/    " + classInfo.getClassName() + "         " + classInfo.getDayTime() + "       " + availability + samePersonAlert);

        }
    }

    public void showAvailableClasses(ArrayList<TempClassInfo> classInfos, String className) {

        System.out.println("\nAvailable classes for next one month from current date of" + className + " class");

        System.out.println("ID---Class Name------Date----------WeekDay-------Time---------Available Seats");

        int count = 1;
        for (TempClassInfo classInfo : classInfos) {
            String availability = "";
            if ((4 - classInfo.getCapacityAllocated()) <= 0) {
                availability = "Full Booked!";
            } else {
                availability = "available seats = " + (4 - classInfo.getCapacityAllocated());
            }

            String samePersonAlert = "";
            if (classInfo.isIsSamePersonTryToBook()) {
                samePersonAlert = " (You already booked this class)";
            }

            System.out.print(count++ +"/" + Constant.mapSpace.get( 4 - String.valueOf(count).length()));
            System.out.print(classInfo.getClassName()  + Constant.mapSpace.get( 16 - classInfo.getClassName().length()));
            System.out.print(classInfo.getDate()  + Constant.mapSpace.get( 14 - classInfo.getDate().length()));
            System.out.print(classInfo.getWeekDay()  + Constant.mapSpace.get( 14 - classInfo.getWeekDay().length()));
            System.out.print(classInfo.getDayTime()  + Constant.mapSpace.get( 13 - classInfo.getDayTime().length()));
            System.out.print(availability + samePersonAlert + "\n");
            //System.out.println(count++ + "/    " + classInfo.getClassName() + "         " + classInfo.getDate() + "    " + classInfo.getWeekDay() + "    " + classInfo.getDayTime() + "       " + availability + samePersonAlert);

        }
    }

    public void printClassNames(HashMap<String, Integer> mapClassNameToPrice) {

        System.out.println("\nID------Class Name------price");
        int counter = 1;

        for (Map.Entry<String, Integer> entry : mapClassNameToPrice.entrySet()) {
            // System.out.println(entry.getKey() + " = " + entry.getValue());

            System.out.print(counter +"/" + Constant.mapSpace.get( 7 - String.valueOf(counter).length()));
            System.out.print(entry.getKey() + Constant.mapSpace.get( 16 - entry.getKey().length()));
            System.out.print(entry.getValue() + "$\n");
            //System.out.println(counter + "/       "+ entry.getKey() + "       " + entry.getValue());
            
            counter++;
        }

    }

}
