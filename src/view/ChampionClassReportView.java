/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import AppData.Constant;
import AppData.Database;
import java.util.HashMap;

/**
 *
 * @author md.jubayer
 */
public class ChampionClassReportView extends BaseView{
    
    public void showChampionClassReport(HashMap<Integer, Float> map, int highestIncomeId, int secondHighestIncomeId){
    
    
        System.out.println("Class Name-----------Total Income");
        
         System.out.print("1/ " +Database.mapExcerciseClassAgaistId.get(highestIncomeId).getName() + Constant.mapSpace.get( 20 - Database.mapExcerciseClassAgaistId.get(highestIncomeId).getName().length()));
         System.out.print(map.get(highestIncomeId) + "$ (Champion)\n");
         
         
         System.out.print("2/ " +Database.mapExcerciseClassAgaistId.get(secondHighestIncomeId).getName() + Constant.mapSpace.get( 20 - Database.mapExcerciseClassAgaistId.get(secondHighestIncomeId).getName().length()));
         System.out.print(map.get(secondHighestIncomeId) + "$ (Runner Up)\n");
         printAlert("Please note that above report is generated based on classes which already attended.");
       // System.out.println("1/ " +Database.mapExcerciseClassAgaistId.get(highestIncomeId).getName()  + "               " + map.get(highestIncomeId) + "$ (Champion)");
        //System.out.println("2/ " +Database.mapExcerciseClassAgaistId.get(secondHighestIncomeId).getName()  + "             " + map.get(secondHighestIncomeId) + "$") ;
    }
    
     public void showChampionClassReport(HashMap<Integer, Float> map){
    
        Integer highestIncomeId = (Integer) map.keySet().toArray()[0];
    
        System.out.println("Class Name-----------Total Income");
        
        System.out.print("1/ " +Database.mapExcerciseClassAgaistId.get(highestIncomeId).getName() + Constant.mapSpace.get( 20 - Database.mapExcerciseClassAgaistId.get(highestIncomeId).getName().length()) + "\n");
        printAlert("Please note that above report is generated based on classes which already attended.");
    }
    
}
