/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import AppData.Database;
import java.util.ArrayList;
import model.Booking;
import model.ClassTimeTable;

/**
 *
 * @author md.jubayer
 */
public class ChangeBookingView extends BaseView{
    
    public void showUserBookedClasses(ArrayList<Booking> bookings){
    
        
        System.out.println("Id-----Date------Day-------Time--------ClassName");
       
        int count = 1;
        for(Booking booking: bookings){
        
             ClassTimeTable classInfo = Database.mapExcerciseClassTimeTablesAgaistId.get(booking.getClassTimeTableId());
             System.out.println(booking.getId() + "/   " + booking.getDate() + "  " + classInfo.getDayName() + "   " + classInfo.getTime() + "   " + Database.mapExcerciseClassAgaistId.get(classInfo.getClassId()).getName());
        
        }
    
    }
    
    public void changeBookingOption(){
    
    
        System.out.println("1/ Replace with another class");
        System.out.println("2/ Cancel this class");
    }
    
}
