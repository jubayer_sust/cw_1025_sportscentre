/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

/**
 *
 * @author md.jubayer
 */
public class MainMenuView extends BaseView {
    
    public void showMainMenus(){
        clearTop();

        System.out.println("\nMain Menu options : ");
        
        System.out.println("1. Book a group excercise");
        
        System.out.println("2. Change a booking");
        
        System.out.println("3. Attend a class");
        
        System.out.println("4. Monthly Class Report");
        
        System.out.println("5. Champion Class Report");
        
        System.out.println("6. Exit");
    
    }

   
}
