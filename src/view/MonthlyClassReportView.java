/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import AppData.Constant;
import AppData.Database;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import model.Booking;
import model.ClassTimeTable;
import model.ExcerciseClass;

/**
 *
 * @author md.jubayer
 */
public class MonthlyClassReportView extends BaseView {
    
    public void showMonthlyReport(HashMap<Integer, ArrayList<Booking>> map) {
        
        System.out.println("Class Name------Total Student----Average Rating");
        for (Map.Entry<Integer, ArrayList<Booking>> entry : map.entrySet()) {
            // System.out.println(entry.getKey() + " = " + entry.getValue());
            
            ExcerciseClass excerciseClass = Database.mapExcerciseClassAgaistId.get(entry.getKey());
            
             System.out.print(excerciseClass.getName() + Constant.mapSpace.get( 19 - excerciseClass.getName().length()));
            //System.out.print(excerciseClass.getName() + "               ");
            
            float avg_rating = 0;
             float total_rating = 0;
            for(Booking booking: entry.getValue()){
            
            
                total_rating = total_rating + Integer.parseInt(booking.getRating());
            }
            
            avg_rating = total_rating / entry.getValue().size();
            
             System.out.print(entry.getValue().size() + Constant.mapSpace.get( 17 - String.valueOf(entry.getValue().size()).length()));
             System.out.print(new DecimalFormat("##.##").format(avg_rating)  + "\n");
            //System.out.print(entry.getValue().size() + "                  " + avg_rating + "\n");
            
            //System.out.println("");
        }
    }
    
}
