/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AppData;

import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author md.jubayer
 */
public class ReadJsonFileTest {
    
    ReadJsonFile instance ;
    
    public ReadJsonFileTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
       instance = new ReadJsonFile();
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Test of readUserJsonFile method, of class ReadJsonFile.
     */
    @Test
    public void testUserJsonFileDoesExist(){
        System.out.println("does userJsonFile Exist");
        File tempFile = new File(ReadJsonFile.UserJsonFileName);
        boolean exists = tempFile.exists();
        assertTrue(exists);
    
    }
    
    /**
     * Test of readUserJsonFile method, of class ReadJsonFile.
     */
    @Test
    public void testExcerciseJsonFileDoesExist(){
        System.out.println("does ExcerciseJsonFile Exist");
        File tempFile = new File(ReadJsonFile.ExcerciseClassFileName);
        boolean exists = tempFile.exists();
        assertTrue(exists);
    
    }
    
    /**
     * Test of readUserJsonFile method, of class ReadJsonFile.
     */
    @Test
    public void testBookingInfoJsonFileDoesExist(){
        System.out.println("does bookingInfoFile Exist");
        File tempFile = new File(ReadJsonFile.BookingInfoFileName);
        boolean exists = tempFile.exists();
        assertTrue(exists);
    
    }
    
    /**
     * Test of readUserJsonFile method, of class ReadJsonFile.
     */
    @Test
    public void testClassTimeTableJsonFileDoesExist(){
        System.out.println("does classTimeTable Exist");
        File tempFile = new File(ReadJsonFile.ClassTimeTableFileName);
        boolean exists = tempFile.exists();
        assertTrue(exists);
    
    }

    /**
     * Test of readUserJsonFile method, of class ReadJsonFile.
     */
    @Test
    public void testReadUserJsonFileWithNullCallBack() {
        System.out.println("readUserJsonFile with null call back");
        ReadJsonFile.JsonFileReadCallBack callback = null;
       
        try{
          instance.readUserJsonFile(callback);
          
        
        }catch(NullPointerException e){
            fail("The readUserJsonFile method does not handle null call back method");
        }
        
        // TODO review the generated test code and remove the default call to fail.
        
    }

    /**
     * Test of readExcerciseClass method, of class ReadJsonFile.
     */
    @Test
    public void testReadExcerciseClass() {
        System.out.println("readExcerciseClass with null callback");
        ReadJsonFile.JsonFileReadCallBack callback = null;
        
        
        try{
          instance.readExcerciseClass(callback);
          
        
        }catch(NullPointerException e){
            fail("The readExcerciseClass method does not handle null call back method");
        }
    }

    /**
     * Test of readClassTimeTableJsonFile method, of class ReadJsonFile.
     */
    @Test
    public void testReadClassTimeTableJsonFile() {
        System.out.println("readClassTimeTableJsonFile with null call back");
        ReadJsonFile.JsonFileReadCallBack callback = null;
        
        
        try{
          instance.readClassTimeTableJsonFile(callback);
          
        
        }catch(NullPointerException e){
            fail("The readClassTimeTableJsonFile method does not handle null call back method");
        }
    }

    /**
     * Test of readBookingInfoJsonFile method, of class ReadJsonFile.
     */
    @Test
    public void testReadBookingInfoJsonFile() {
        System.out.println("readBookingInfoJsonFile with null call back");
        ReadJsonFile.JsonFileReadCallBack callback = null;
        
        
        try{
          instance.readBookingInfoJsonFile(callback);
          
        
        }catch(NullPointerException e){
            fail("The readBookingInfoJsonFile method does not handle null call back method");
        }
    }
    
}
