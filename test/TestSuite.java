/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author md.jubayer
 */
import AppData.ReadJsonFileTest;
import controller.BookClassControllerTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   ReadJsonFileTest.class,
   BookClassControllerTest.class
})

public class TestSuite {   
}