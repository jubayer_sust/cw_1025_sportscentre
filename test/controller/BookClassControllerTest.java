/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import AppData.Constant;
import AppData.Database;
import model.TempClassInfo;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author md.jubayer
 */
public class BookClassControllerTest {

    private BookClassController controller;

    public BookClassControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        Database database = new Database();
        database.loadDatabaseFromJson(new Database.databaseLoadCallBack() {
            @Override
            public void onDatabaseLoad() {

                //Logged in as a registered user whose id is 1
                Constant.userId = 1;
                //Load the book class controller
                controller = new BookClassController();

            }
        });

    }

    @After
    public void tearDown() {
    }

    /**
     * This unit test is for testing bookClass method whether this method can
     * able to book a certain class.
     */
    @Test
    public void testBookAClass() {

        System.out.println("Test book a class");

        /**
         * Lets make a temporary class info which we will use to book a class.
         * The class info which we will book must be book-able. Note that, the
         * system will not store this booking information in the json file. As a
         * result each time we run this unit test, it should be successful.
         */
        //1st class info
        int classId = 1; // classId = 1 = Yoga class
        int classTimeTableId = 2; // classTimeTableId = 1 = Saturday Morning
        String date = "07/01/2017"; // This is saturday. There is no entry for this date in database

        TempClassInfo tempClassinfo = new TempClassInfo(classId, classTimeTableId, date);

        int response = controller.bookAClass(tempClassinfo, null);
        int expected = 1;

        /**
         * As database does not have any entry for tempClassInfo so the system
         * should allow book this classInfo. If it doesn't allow then system is
         * not ready. if the response is 1 then it means system booked the
         * class.
         */
        assertEquals(expected, response);

    }

    /**
     * Test the bookAClass method, of BookClassController.java. This unit test
     * tested the condition whether same user can book same class of same time
     * slot.
     */
    @Test
    public void testBookSameClassTwiceFromSameUser() {
        System.out.println("\nTest book same class from same user");

        // UserListView userListView = new UserListView();
        /**
         * Lets make a temporary class info which we will use to book a class.
         * The class info which we will book must be book-able.
         */
        //1st class info
        int classId = 1; // classId = 1 = Yoga class
        int classTimeTableId = 3; // classTimeTableId = 3 = Sunday Morning
        String date = "08/01/2017"; // This is Sunday. There is no entry for this date in database

        TempClassInfo tempClassinfo = new TempClassInfo(classId, classTimeTableId, date);

        int responseFirstEntry = controller.bookAClass(tempClassinfo, null);

        int firstEntryExpected = 1;
        assertEquals(firstEntryExpected, responseFirstEntry);

        /**
         * Now lets try to book the same class. The method should return 2 as
         * because we are trying to book same class from same user. Here
         * response 2 means the class already booked.
         */
        int responseSecondEntry = controller.bookAClass(tempClassinfo, null);
        int secondEntryExpected = 2;
        assertEquals(responseSecondEntry, secondEntryExpected);

    }

    /**
     * This test for bookAClass method of BookClassController. This unit test is
     * for testing the condition, whether a user can book different type class
     * on same time slot. The system shouldn't allow this type of booking.
     */
    @Test
    public void testBookDifferentClassOnSameTimeSlot() {
        System.out.println("\n Test book different type class at same time slot from same user.");
        /**
         * Lets make a temporary class info which we will use to book a class.
         * The class info which we will book must is must be bookable. Later we
         * will make another class info which time slot will be same but
         * different category class. With the 2nd class info if we want to book
         * a class then my program should give an error.
         */
        //1st class info
        int classId = 1; // classId = 1 = Yoga class
        int classTimeTableId = 1; // classTimeTableId = 1 = Saturday Morning
        String date = "28/01/2017"; // This is saturday. There is no entry for this date in database

        TempClassInfo tempClassinfoFirst = new TempClassInfo(classId, classTimeTableId, date);

        //2nd class info
        int classId2 = 2; // classId = 2 = Zumba class (Different class, previous entry was Yoga)
        int classTimeTableId2 = 6; // classTimeTableId = 6 = Saturday Morning (Same time slot as previous entry)
        String date2 = "28/01/2017"; // This is same date as previous entry

        TempClassInfo tempClassinfoSecond = new TempClassInfo(classId2, classTimeTableId2, date2);

        /**
         * if response is 1 that means system booked the class for the user. Now
         * lets try book 2nd class using tempClassInfoSecond which is actually
         * same time slot but different class
         */
        int firstEntryResponse = controller.bookAClass(tempClassinfoFirst, null);
        /**
         * as we are trying to book different type class in same time slot on
         * same date then the system shouldn't allow user to book that class.
         * And the system should return error code 2.
         */
        int secondEntryResponse = controller.bookAClass(tempClassinfoSecond, null);
        int expectedResponse = 2;

        assertEquals(expectedResponse, secondEntryResponse);

    }

}
